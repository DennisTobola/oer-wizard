var license_1 = {"l1": "CC-0"}
var license_2 = {"l1": "CC-BY"}
var license_3 = {"l1": "CC-BY-SA"}
var license_4 = {"l1": "CC-BY-NC-SA"}
var license_5 = {"l1": "CC-BY-NC"}
var license_6 = {"l1": "Urheberrechtlich geschützt/ Copyright"}
var license_7 = {"l1": "CC-PD-MARK"}
var license_8 = {"l1": "CC-BY-ND"}
var license_9 = {"l1": "CC-BY-NC-ND"}


var license_10 = {"l1": "CC-BY-NC-ND 4.0"}
var license_11 = {"l1": "CC-BY 4.0"}
var license_12 = {"l1": "CC-BY-SA 4.0"}
var license_13 = {"l1": "CC-BY-NC-SA 4.0"}
var license_14 = {"l1": "CC-BY-NC 4.0"}
var license_15 = {"l1": "CC-0"}
var license_16 = {"l1": "CC-BY-ND 4.0"}

//copyright
var copyright_1 = "Ihre Bildungsressource wird ausschließlich aus eigenen Inhalten bestehen. Daher können Sie diese nach ihren eigenen Bedürfnissen lizenzieren. Im Sinne der OER Praxis empfehlen wir, eine möglichst offene Lizenz zu wählen (public domain, CC 0, CC BY und CC BY SA). Neben der maschinellen Auslesbarkeit der Lizenzen, die Sie im OER-Portal im Einstellungsprozess realisieren, sollten Sie die Lizenzhinweise bereits bei der Erstellung deutlich sichtbar auf Ihrem Bildungsmaterial angeben.";

var copyright_2 = "Prüfen Sie zunächst an Hand der Lizenztexte, ob Sie berechtigt sind, die fremden Materialien oder Inhalte zu verwenden bzw. einzubinden. Geben Sie die von der urhebenden Person gemachten Angaben korrekt wieder.\n" +
		"</br></br><strong>Hinweise für die Wiederveröffentlichung von fremden Materialien:</strong></b></br>" +
		"Steht das von Ihnen verwendete fremde Werk nicht allein, sondern wird von Ihnen sichtbar gerahmt, dann können Sie mithilfe eines Textbausteins auf die Eigenständigkeit der Werke hinweisen.  z.B. Dieses Werk und dessen Inhalte sind - sofern nicht anders angegeben-  lizenziert unter (um Lizenzangaben ergänzen). Dies gilt aber nicht für Materialien, die urheberrechtlich geschützt sind oder ein Copyright aufweisen. Positionieren Sie diesen Textbaustein gut sichtbar auf Ihrem Werk!" +
		"Die Lizenzwahl richtet sich andernfalls am verwendeten CC-lizenzierten Werk mit der höchsten Einschränkung. Bei Unsicherheiten können Sie den CC Mixer heranziehen, um die entsprechende Lizenz anzugeben.";

var copyright_3 = "Prüfen Sie zunächst an Hand der Lizenztexte, ob Sie berechtigt sind, die fremden Materialien oder Inhalte zu verändern bzw. weiterzuentwickeln. Geben Sie die von der urhebenden Person gemachten Angaben korrekt an.</br></br>\n" +
"<ol><li>	Geben Sie die Lizenzangaben der urhebenden Personen in unmittelbarer Nähe zum Werk an.</li>"+
"<li>	Beim Vermischen von mehreren Ressourcen mit unterschiedlichen Lizenzen müssen Sie bei der Wiederveröffentlichung Ihres Werkes (Sammelwerk) darauf achten, sich an dem Werk mit der größten Lizenzeinschränkung zu orientieren und entsprechend zu veröffentlichen.</li></ol>"+
	"<strong>Hinweise für die Wiederveröffentlichung von fremden Materialien:</strong></b></br>" +
	"</br>Steht das von Ihnen verwendete fremde Werk nicht allein, sondern wird von Ihnen sichtbar gerahmt, dann können Sie mithilfe eines Textbausteins auf die Eigenständigkeit der Werke hinweisen.  z.B. Dieses Werk und dessen Inhalte sind - sofern nicht anders angegeben-  lizenziert unter (um Lizenzangaben ergänzen). Dies gilt aber nicht für Materialien, die urheberrechtlich geschützt sind oder ein Copyright aufweisen. Positionieren Sie diesen Textbaustein gut sichtbar auf Ihrem Werk!" +
	"Die Lizenzwahl richtet sich andernfalls am verwendeten CC-lizenzierten Werk mit der höchsten Lizenzeinschränkung. Bei Unsicherheiten können Sie den CC Mixer heranziehen, um die entsprechende Lizenz anzugeben.";

var copyright_4 = "Prüfen Sie zunächst an Hand der Lizenztexte, ob Sie berechtigt sind, das fremde Material/die fremden Inhalte zu verändern und erneut zu veröffentlichen.</br></br>\n" +
"<ol><li>Geben Sie die von der urhebenden Person gemachten Angaben korrekt und vollständig an.</li>"+
"<li>Ergänzen Sie diese mit der von Ihnen vorgenommenen Änderung des Originals (Art der Veränderung z.B. Ergänzung, Designanpassung).</li>"+
"<li>Je nach Lizenztext ergänzen Sie Ihren Namen (Vorname Nachname bzw. Institution oder Pseudonym).</li>"+
"<li>Geben Sie entweder den übernommenen bzw. rechtmäßig veränderten Lizenztext an.</li></ol>"+
	"<strong>Hinweise für die Wiederveröffentlichung von fremden Materialien:</strong></br>" +
	"</br>Steht das von Ihnen verwendete fremde Werk nicht allein, sondern wird von Ihnen sichtbar gerahmt, dann können Sie mithilfe eines Textbausteins auf die Eigenständigkeit der Werke hinweisen.  z.B. Dieses Werk und dessen Inhalte sind - sofern nicht anders angegeben-  lizenziert unter (um Lizenzangaben ergänzen). Dies gilt aber nicht für Materialien, die urheberrechtlich geschützt sind oder ein Copyright aufweisen. Positionieren Sie diesen Textbaustein gut sichtbar auf Ihrem Werk!" +
	"Die Lizenzwahl richtet sich andernfalls am verwendeten CC-lizenzierten Werk mit der höchsten Lizenzeinschränkung. Bei Unsicherheiten können Sie den CC Mixer heranziehen, um die entsprechende Lizenz anzugeben.";



//license
var cc0_a = "Sie dürfen das Material uneingeschränkt nutzen und wiederveröffentlichen. Geben Sie lediglich so nah wie möglich am entsprechenden Material/Inhalt die Lizenzangabe CC 0 an, damit erkennbar bleibt, dass es sich um ein gemeinfreies Werk handelt.</br></br>" +
"Mit dem Lizenztext CC 0 erlauben Sie anderen Ihr Werk unbeschränkt ohne Bedingungen zu verwenden. Es darf also kopiert, veröffentlicht, weiterbearbeitet und verändert wieder bereitgestellt werden. Aus Wertschätzung wird in der Regel trotzdem die urhebende Person genannt. Kopien und bearbeitete Versionen können auch kommerziell genutzt werden." +
"</br></br>Die Angaben zu den fremden Inhalten oder Materialien sollten verpflichtend mitgeführt werden:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) und ggf. Titel können optional angegeben werden,</li>" +
"<li>Lizenztext CC-0 mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by = "Sie dürfen das Material uneingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben.</br></br>" +
"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) kann optional angegeben werden,</li>" +
"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
"<li>Vorname und Nachname der urhebenden Person bzw. gewünschter Name,</li>" +
"<li>Lizenztext CC-BY mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by_sa = "Sie dürfen das Material uneingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben und eine Wiederveröffentlichung unter gleichen Bedingungen erfolgt.</br></br>" +
"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) kann optional angegeben werden,</li>" +
"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
"<li>Vorname und Nachname der urhebenden Person bzw. gewünschter Name,</li>" +
"<li>Lizenztext CC-BY-SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by_nc = "Sie dürfen das Material eingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben und es nicht für kommerzielle Zwecke nutzen (Entgelt für die Nutzung verlangen u.a. Workshopgebühren).</br></br>" +
"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) kann optional angegeben werden,</li>" +
"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
"<li>Vorname und Nachname der urhebenden Person bzw. gewünschter Name,</li>" +
"<li>Lizenztext CC-BY-NC mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by_nd = "Sie dürfen das Material nur eingeschränkt nutzen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben. Eine Bearbeitung ist nicht erlaubt.</br></br>" +
"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) kann optional angegeben werden,<li>" +
"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
"<li>Vorname und Nachname der urhebenden Person bzw. gewünschter Name,</li>" +
"<li>Lizenztext CC-BY-NC mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by_nc_sa = "Sie dürfen das Material nur eingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben und es nicht für kommerzielle Zwecke nutzen (Entgelt für die Nutzung verlangen u.a. Workshopgebühren) und solange das neu entstandene Werk unter den gleichen Bedingungen weitergegeben bzw. veröffentlicht wird.</br></br>" +
"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) kann optional angegeben werden,</li>" +
"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
"<li>Vorname und Nachname der urhebenden Person bzw. gewünschter Name,</li>" +
"<li>Lizenztext CC-BY-NC-SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by_nc_nd = "Sie dürfen das Material nur eingeschränkt nutzen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben und es nicht für kommerzielle Zwecke nutzen (Entgelt für die Nutzung verlangen u.a. Workshopgebühren) und keine Bearbeitungen vornehmen.</br></br>" +
"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) kann optional angegeben werden,</li>" +
"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
"<li>Vorname und Nachname der urhebenden Person bzw. gewünschter Name,</li>" +
"<li>Lizenztext CC-BY-NC-ND mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var copy1 = "Materialien oder Inhalte, die urheberrechtlich geschützt sind, dürfen weder kopiert, vermischt, bearbeitet, noch von Ihnen (wieder-)veröffentlicht werden. Inhalte Dritter dürfen im Rahmen der gesetzlichen Schranken des Urheberrechts rechtskonform verwendet werden, insbesondere die redliche Verwendung im Rahmen der wissenschaftlichen Praxis, z.B. die Zitatfreiheit nach § 51 UrhG.</br></br>" +
"Wir empfehlen im OER Portal nach alternativen Bildungsressourcen zu suchen, die eine ausdrückliche Nachnutzung erlauben.";

var cc_pd_mark = "Prüfen Sie genau, ob es sich bei den fremden Materialien oder Inhalten um gemeinfreie Werke handelt. Ist das der Fall, dann dürfen Sie das Material uneingeschränkt nutzen und wiederveröffentlichen. Geben Sie lediglich, so nah wie möglich am entsprechenden Material/Inhalt die Lizenzangabe Public Domain Mark (CC-PD-MARK) an, damit erkennbar bleibt, dass es sich um ein gemeinfreies Werk handelt.</br></br>" +
"Die Angaben der verwendeteten Bildungsressourcen sollten mitgeführt werden:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) und ggf. Titel können optional angegeben werden,</li>" +
"<li>Lizenztext Public Domain Mark mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";






var cc0_b = "<strong>Sie möchten Ihre Inhalte uneingeschränkt zur freien Nutzung zur Verfügung stellen?</strong> Dann empfehlen wir den Lizenztext CC-BY.</br></br>" +
		"Mit dem Lizenztext CC 0 erlauben Sie anderen Ihr Werk unbeschränkt ohne Bedingungen zu verwenden. Es darf also kopiert, veröffentlicht, weiterbearbeitet und verändert wieder bereitgestellt werden. Aus Wertschätzung wird in der Regel trotzdem die urhebende Person genannt. Kopien und bearbeitete Versionen können auch kommerziell genutzt werden." +
		"</br></br>So beschriften Sie Ihre Bildungsressource eindeutig:</br></br>" +
		"<ol><li>Werkbezeichnung (z.B. /Bild/Text/Video) und Titel können optional angegeben werden,</br>" +
		"<li>Lizenztext CC 0 mit Verlinkung auf Creative Commons Homepage + Lizenzversion,</br>" +
		"<li>ggf. Lizenzicon mit der Attributation Zero einfügen.</li></ol>";

var cc_by_4 = "<strong>Sie möchten sicherstellen, dass die Herkunft sichtbar bleibt?</strong> Dann empfehlen wir den Lizenztext CC-BY.</br></br>" +
"Mit dem Lizenztext CC BY erlauben Sie anderen Ihr Werk unbeschränkt unter der Bedingung, dass Ihr angegebenr Name korrekt aufgeführt wird, zu verwenden. Es darf also kopiert, weiterbearbeitet, verändert oder unverändert veröffentlicht werden. Sie erlauben auch Kopien und bearbeitete Versionen kommerziell zu nutzen." +
"</br></br>So beschriften Sie Ihre Bildungsressource eindeutig:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
"<li>Vornamen und Nachnamen ggf. Institution bzw. Pseudonym,</br>" +
"<li>Lizenztext CC BY mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
"<li>ggf. Lizenzicon mit der Attributation Namensnennung einfügen.</li></ol>";

var cc_by_sa_4 = "<strong>Sie wollen sicherstellen, dass die Herkunft sichtbar bleibt und Ihre Inhalte auch weiterhin frei zugänglich bleiben?</strong> Dann empfehlen wir den Lizenztext CC-BY-SA.</br></br>" +
"</br></br>So beschriften Sie Ihre Bildungsressource eindeutig:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
"<li>Vornamen und Nachnamen ggf. Institution bzw. Pseudonym,</br>" +
"<li>Lizenztext CC BY SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
"<li>ggf. Lizenzicon mit den Attributationen Namensnennung - Weitergabe unter gleichen Bedingungen einfügen.</li></ol>";

var cc_by_nc_4 = "<strong>Sie wollen sicherstellen, dass die Herkunft sichtbar bleibt und Ihre Bildungsressource nicht für kommerzielle Zwecke verwendet wird</strong>, dann verwenden Sie den Lizenztext CC-BY-NC.</br></br>" +
"Die Einschränkung NC bringt einige Nachteile für andere bei der Nachnutzung. In der Praxis könnte das z.B. bedeuten, dass Ihre Bildungsressource im Rahmen eines Workshops im Hochschulkontext von Dritten nicht zum Einsatz kommen darf." +
"</br></br>So lizenzieren Sie Ihre Bildungsressource eindeutig:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
"<li>Vornamen und Nachnamen ggf. Institution bzw. Pseudonym,</li>" +
"<li>Lizenztext CC BY NC mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
"<li>ggf. Lizenzicon mit den Attributationen Namensnennung - Nicht kommerziell einfügen.</li></ol>";

var cc_by_nc_sa_4 = "<strong>Sie wollen sicherstellen, dass die Herkunft sichtbar bleibt und Ihre Bildungsressource nicht für kommerzielle Zwecke verwendet wird und jede weitere Bearbeitung unter gleichen Bedingungen erfolgt,</strong> dann verwenden Sie folgenden Lizenztext: CC-BY-NC-SA 4.0.</br></br>" +
"Die Einschränkung NC bringt einige Nachteile für andere bei der Nachnutzung. In der Praxis könnte das z.B. bedeuten, dass Ihre Bildungsressource z. B. im Rahmen eines Workshops im Hochschulkontext von Dritten nicht verwendet werden darf." +
"</br></br>So beschriften Sie Ihre Bildungsressource eindeutig:</br></br>" +
"<ol><li>Werkbezeichnung (z.B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
"<li>Vornamen und Nachnamen ggf. Institution bzw. Pseudonym,</li>" +
"<li>Lizenztext CC BY NC SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
"<li>ggf. Lizenzicon mit den Attributationen Namensnennung - Nicht kommerziell - Weitergabe unter gleichen Bedingungen einfügen.</li></ol>";

var cc_by_nd_4 = "<strong>Sie wollen sicherstellen, dass die Herkunft sichtbar bleibt und eine Bearbeitung unterbleibt </strong>, dann verwenden Sie folgenden Lizenztext CC-BY-ND 4.0. Diese Lizenzwahl bringt einige Nachteile für andere bei der Nachnutzung, da die Möglichkeit einer Anpassung an den eigenen Lehrkontext nicht gegeben ist.</br></br>";

var cc_by_nc_nd_4 = "<strong>Sie wollen sicherstellen, dass die Herkunft sichtbar bleibt, eine Bearbeitung unterbleibt Ihre Bildungsressource nicht für kommerzielle Zwecke verwendet wird,</strong> dann verwenden Sie folgenden Lizenztext CC-BY-NC-ND 4.0</br></br>" +
		"Die Einschränkungen NC und ND bringen einige Nachteile für andere bei der Nachnutzung. Denn eine Bearbeitung und Anpassung an den eigenen Kontext wird verweigert und die Nutzung könnte in der Praxis bedeuten, dass Ihre Bildungsressource z. B im Rahmen eines Workshops im Hochschulkontext von Dritten nicht verwendet werden darf.";


