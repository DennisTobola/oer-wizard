# OER Wizard

Prototyp des [Wizards](https://tibhannover.gitlab.io/oer/oer-wizard/html/wizard-modal.html)

## Beispiele

### Outcome Skript mit integrierten Fremdmaterialien

**Formular**

```
1. Was möchten Sie erstellen?
  (O) Informations- oder Medienobjekt
  ( ) Lernobjekt
  ( ) Lehreinheit
  ( ) Kurs

2a Welchen Medientypen möchten Sie erstellen?
  (O) Text
  ( ) Video
  ( ) Audio
  ( ) interaktives Material
  ( ) Bild
  ( ) Tool
add
     Welches Medienformat möchten Sie erstellen?
  ( ) Lehrtext
  (O) Skript
  ( ) Poster
  ( ) Podcast
  ( ) Infografik
  ( ) Folien
  
  
2b Welche Lernhandlung sollen Studierende primär durchführen?
  ( ) Materialien/Lerninhalte rezipieren
  ( ) Materialien/Lerninhalte analysieren
  ( ) Materialien/Lerninhalte festigen, üben
  ( ) Eigenes Produzieren
  ( ) Prozesse reflektieren/evaluieren
  
add 
     Welche Quellen/Materialien werden für die Lernhandlung hinzugezogen?
  ( ) Text 
  ( ) Video
  ( ) Audio
  ( ) interaktives Material
  ( ) Bild
  ( ) Tool
  
2c Welche Phase möchten Sie planen?
  ( ) Kontaktaufnahme 
  ( ) Einstiegsphase
  ( ) Vermittlungsphase 
  ( ) Präsentationsphase
  ( ) interaktve Phase
  ( ) Reflexionsphase
  ( ) Evaluationsphase
  ( ) Transferphase 
  ( ) Prüfungsphase
  
add
     Wie möchten Sie das Lernszenario gestalten?
  ( ) Präsenzphase
  ( ) Onlinephase
add  
  ( ) synchron
  ( ) asynchon
  
2d Welchen Ansatz möchten Sie planen?
  ( ) Problembasiertes Lernen
  ( ) Forschendes Lernen
  ( ) Blended Learning
  ( ) ...
  
3. Planen Sie fremde Materialien zu integrieren?
  ( ) nein, ich werde ausschließlich eigene Inhalte erstellen
  ( ) ja, ich plane fremde Inhalte, die mit CC Lizenzen versehen sind, einzubinden/zu verwenden
  (O) ja, ich plane fremde Inhalte, die mit einem Copyright versehen sind, einzubinden/zu verwenden
  ( ) ja, ich plane fremde Inhalte einzubinden/zu verwenden, deren Nutzungsrechte nicht erkenntlich sind
  
4. Mit welchen Bestimmungen ist das verwendete (Fremd-)Material  versehen?
  ( ) CC 0
  ( ) CC BY
  (O) CC BY SA
  ( ) CC BY NC
  ( ) CC BY NC SA
  ( ) CC BY NC ND
  
```


**Ausgabe**

Individueller Leitfaden für die Erstellung von Skripten

**1.1 (Informationsobjekt) Text -> (Metadaten)** 

Machen Sie Ihr Material auffindbar, in dem Sie es mit zusätzlichen Beschreibungen versehen. Hier können Sie schon während des Erstellungsprozesses wichtige Schlagworte festhalten. Solche Beschreibungen stellen für andere Nutzer:innen einen Mehrwert dar, da andere Lehrende a) Ihr Skript besser finden können und b) die Passung Ihres Skriptes in den eigenen Lehr-/Lernkontext besser einschätzen können. 

**Move: Machen Sie das Skript auffindbar!**

**Step:** Legen Sie Schlagworte und Intentionen fest, die Sie später beim Einstellen ins OERnds Portal für die Beschreibung verwenden können.



**2a.1 (Text) Text -> (Werkzeug)** 

Damit andere Lehrende Ihr Skript bearbeiten und nachnutzen können, müssen die  von Ihnen genutzten Tools oder Textverarbeitungsprogramme gut zugänglich sein. Dies kann bereits dadurch geschehen, dass Sie bei der Erstellung des Skriptes auf Open Source Werkzeuge zurückgreifen oder weit verbreitete Textverarbeitungsprogramme nutzen. 
Das OERnds Portal empfiehlt für die Verschriftlichung und Bereitstellung Ihres Skriptes die Nutzung von Gitlab. 
(Gitlab logo) Wie Sie Materialien in Gitlab anlegen, lesen Sie hier (Link Tutorial Gitlab: Vorteile: (Aufzählung der Vorteile) + Versionierung, + Einbindung von Grafiken, + Einbettung von Aufgaben und fremden Materialien + markdown --> Generierung des Material in vers. Dateiformate bei der Nutzung von TIB Vorlagen (LINK SKRIPT vorlage), Nachteil: (Aufzählung der Nachteile)

**--> (Dateiformate)** 

Damit andere Ihr Material nutzen und verändern können, sollten Sie bei der Erstellung darauf achten, dass Sie für Ihr Skript ein Dateiformat verwenden, das ohne Zugangsbeschränkungen erreichbar ist  und eine Bearbeitung durch andere Personen ermöglicht. Geeignete Dateiformate für Skripte sind: odt, docx, md, html,?

**Move: Machen Sie Ihr Skript zugänglich!**

**Step:** Nutzen Sie Open Source Werkzeuge, wie Libre office oder open office, **or**

**Step:** Nutzen Sie weit verbreitete Textverarbeitungsprogramme (MS Word), **or**

**Step:** Nutzen Sie Gitlab und die Vorlagen der TIB Hannover,** and**

**Step:** Speichern Sie Ihr Skript als odt ab, **or**

**Step:** Speichern Sie Ihr Skript als docx ab, **or**

**Step:** Nutzen Sie Gitlab und die Vorlage (TIB Hannover) und stellen Sie Ihr Skript in mehreren Dateiformaten zur Verfügung


**2a.8 (Skript) Text -> Didaktik**

Skripte in der Lehre haben eine lange Tradition. Die Inhalte werden von Lehrenden arrangiert und sollen diesen eine Orientierung während der Vorlesung ermöglichen. Für Studierende können Skripte insbesondere der Vorbereitung und Nachbereitung dienen.
Lesen Sie hier (https://www.e-teaching.org/lehrszenarien/vorlesung/skript) mehr über die Funktionen und den Mehrwert von Skripten
Eine kurze Anleitung zum Aufbau von Skripten und eine Textvorlage finden Sie hier (gitlab Vorlage Skripte)

**Move: Bauen Sie Ihr Skript didaktisch so auf, dass es die Studierenden beim Wissenserwerb unterstützt.**

**Step:** Wählen Sie den Titel der Vorlesung als Überschrift **and**

**Step:** Geben Sie in der Einführung einen Überblick über die thematischen Schwerpunkte und bringen Sie diese in einen Zusammenhang **and**

**Step:** Schließen Sie jeden thematischen Abschnitt mit einer kurzen Zusammenfassung, **and/or**

**Step:** Formulieren Sie Aufgaben zu den referierten Inhalten **and/or**

**Step:** Lassen Sie im Layout Platz für Mitschriften/für die Antworten auf gestellte Aufgaben **and/or**

**Step:** Nutzen Sie Visualisierungen, wenn möglich, um Sachverhalte zu verdeutlichen, **and/or**

**Step:**Veranschaulichen Sie den Lernstoff so gut wie möglich mit Beispielen, Analogien, Fällen, etc.


**4.3 (Copyright) Text -> Lizenzwahl**

Es könnte bei der Veröffentlichung Ihres Skriptes zu Urheberrechtsverletzungen kommen, wenn Sie fremde Materialien einsetzen, die mit einem Copyright versehen sind.
Wir empfehlen Ihnen alternative Materialien in Ihrem Skript zu verwenden, die mit einer CC Lizenz versehen sind und die Veröffentlichung durch entsprechende Bestimmungen erlauben. (CC 0, CC BY oder CC BY SA). Hier können Sie nach OER Materialien suchen: (Link OER Suchmaschine)

Sie finden keine Entsprechungen? Dann empfehlen wir Ihnen, die Nutzungsrechte beim Urheber zu erfragen. Dies wird allerdings je nach Absprache mit dem Urheber des fremden Materials zur Konseqzúenz haben, dass Sie Ihr Skript unter CC BY ND bzw. CC BY NC ND lizenzieren müssten.

Alternativ können Sie nach wissenschaftlichen Kriterien, das Material auf das Sie gern zurückgreifen möchten, so darstellen, dass es als Zitat in Erscheinung tritt und es entsprechend nachbilden. Aber auch hier auf die Urheberrechte achten. 

**Move: Wählen Sie eine Lizenz für Ihr Material**

**Step** Überprüfen Sie die Lizenzen der von Ihnen verwendeten Materialien und ob mit einer Veröffentlichung Ihres Materials Urheberrechte Dritter verletzt würden **and**

**Step** Vergeben Sie unter Berücksichtigung von Step 1 eine CC-Lizenz **and/or**

**Step** Legen Sie Titel, Autor:in und Nutzungsbedingungen für Ihr Material fest.



### Lehrtext erstellen ohne fremde Inhalte

**Formular**

```
1. Was möchten Sie erstellen?
  (O) Informations- oder Medienobjekt
  ( ) Lernobjekt
  ( ) Lehreinheit
  ( ) Kurs

2a Welchen Medientypen möchten Sie erstellen?
  (O) Text
  ( ) Video
  ( ) Audio
  ( ) interaktives Material
  ( ) Bild
  ( ) Tool
add
     Welches Medienformat möchten Sie erstellen?
  (O) Lehrtext
  ( ) Skript
  ( ) Poster
  ( ) Podcast
  ( ) Infografik
  ( ) Folien
  
  
2b Welche Lernhandlung sollen Studierende primär durchführen?
  ( ) Materialien/Lerninhalte rezipieren
  ( ) Materialien/Lerninhalte analysieren
  ( ) Materialien/Lerninhalte festigen, üben
  ( ) Eigenes Produzieren
  ( ) Prozesse reflektieren/evaluieren
  
add 
     Welche Quellen/Materialien werden für die Lernhandlung hinzugezogen?
  ( ) Text 
  ( ) Video
  ( ) Audio
  ( ) interaktives Material
  ( ) Bild
  ( ) Tool
  
2c Welche Phase möchten Sie planen?
  ( ) Kontaktaufnahme 
  ( ) Einstiegsphase
  ( ) Vermittlungsphase 
  ( ) Präsentationsphase
  ( ) interaktve Phase
  ( ) Reflexionsphase
  ( ) Evaluationsphase
  ( ) Transferphase 
  ( ) Prüfungsphase
  
add
     Wie möchten Sie das Lernszenario gestalten?
  ( ) Präsenzphase
  ( ) Onlinephase
add  
  ( ) synchron
  ( ) asynchon
  
2d Welchen Ansatz möchten Sie planen?
  ( ) Problembasiertes Lernen
  ( ) Forschendes Lernen
  ( ) Blended Learning
  ( ) ...
  
3. Planen Sie fremde Materialien zu integrieren?
  (O) nein, ich werde ausschließlich eigene Inhalte erstellen
  ( ) ja, ich plane fremde Inhalte, die mit CC Lizenzen versehen sind, einzubinden/zu verwenden
  ( ) ja, ich plane fremde Inhalte, die mit einem Copyright versehen sind, einzubinden/zu verwenden
  ( ) ja, ich plane fremde Inhalte einzubinden/zu verwenden, deren Nutzungsrechte nicht erkenntlich sind
  
4. Mit welchen Bestimmungen ist das verwendete (Fremd-)Material  versehen?
  ( ) CC 0
  ( ) CC BY
  ( ) CC BY SA
  ( ) CC BY NC
  ( ) CC BY NC SA
  ( ) CC BY NC ND
```



**Ausgabe**

Individueller Leitfaden
1.2 -> Text Metadaten
2a.1 add 7 -->Werkzeuge/ Dateiformate
2b -
2c -
2d-
3.1 --> Lizenzinformationen
4  -



-----

### ... erstellen

**Formular**

```


**Ausgabe**

Individueller Leitfaden
1.2 no text
2.3 no text
3.4 Text -> Verweis auf Tutorial

### Video teilen

**Formular**

```

```
